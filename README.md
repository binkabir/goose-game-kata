# Overview
Goose game is a game where two or more players move pieces around a track by rolling a die. The aim of the game is to reach square number sixty-three before any of the other players and avoid obstacles.

# Getting Started
This application was built with scala 2.12.6 

## Build the application
 Step 1: Open a terminal within the application's primary directory and run this command 
``>sbt run`` 
This should compile and run the application. At this, the application allows you to make inputs to the game by showing you a welcome text 

Step 2: Add player(s) to the with the command 
``add player Pippo`` 

Step 3: Add player's moves with this command 
``move Pippo 4,2`` 

You can also randomly generate moves by typing in the terminal ``move Pippo``  

All input returns a message on the command prompt

# License
This code is open source software licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0.html)





