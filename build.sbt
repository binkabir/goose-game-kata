import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.7",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "goose-game-kata",
    libraryDependencies ++= Seq(
      scalaTest % Test, 
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.wvlet" %% "wvlet-log" % "1.2.3"
  ))
