package goosekata

import org.scalatest.WordSpec

class GameSpec  extends  WordSpec{

   "The Game Class" should {
     "add new users to game Map" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
        newGame.addPlayer(player)
       assert(newGame.getBoard == Map(player.name -> Seq(PlayerPosition(Move(0,0),0))))
     }

     "return error when duplicate player name is added" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)
       assert(newGame.getBoard == Map(player.name -> Seq(PlayerPosition(Move(0,0),0))))

       assert(newGame.addPlayer(player) == Left("Pippo: already existing player"))

     }

     "add playerMove to a Game" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)
       assert(newGame.getBoard == Map(player.name -> Seq(PlayerPosition(Move(0,0),0))))

       val playerMoveAction = PlayerMoveAction("Pippo", Move(2,1))
       newGame.movePlayerPosition(playerMoveAction)

       assert(newGame.getBoard == Map(player.name -> Seq(PlayerPosition(Move(2,1),3),PlayerPosition(Move(0,0),0))))
     }

     "return invalid player when an unknown player push to playerMove" in {
       val newGame = new Game
       val playerMoveAction = PlayerMoveAction("Pippo", Move(2,3))
       val error = newGame.movePlayerPosition(playerMoveAction)

       assert(error == "Invalid player")

     }

     "return win if a player have a sum of 63 " in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)
       assert(newGame.getBoard == Map(player.name -> Seq(PlayerPosition(Move(0,0),0))))

       val playerMoveActionList = Seq.fill(5)(PlayerMoveAction("Pippo", Move(6,6)))
       playerMoveActionList.foreach(x => newGame.movePlayerPosition(x))

       val result = Map("Pippo" -> List(PlayerPosition(Move( 6, 6) ,60), PlayerPosition( Move( 6, 6),48), PlayerPosition( Move( 6, 6),36), PlayerPosition(Move( 6, 6),24), PlayerPosition(Move( 6, 6) ,12), PlayerPosition(Move( 0, 0) ,0)))
       assert(newGame.getBoard == result)

       val playerMoveAction2 = PlayerMoveAction("Pippo", Move(1,2))

        val message = newGame.movePlayerPosition(playerMoveAction2)

        assert(message == "Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!")
     }

     "return sum to 61 if the playerPosition is high than 63 " in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveActionList = Seq.fill(5)(PlayerMoveAction("Pippo", Move(6,6)))
       playerMoveActionList.foreach(x => newGame.movePlayerPosition(x))

       val result = Map("Pippo" -> List(PlayerPosition(Move( 6, 6) ,60), PlayerPosition( Move( 6, 6),48), PlayerPosition( Move( 6, 6),36), PlayerPosition(Move( 6, 6),24), PlayerPosition(Move( 6, 6) ,12), PlayerPosition(Move( 0, 0) ,0)))
       assert(newGame.getBoard == result)

       val playerMoveAction2 = PlayerMoveAction("Pippo", Move(3,2))

       val message = newGame.movePlayerPosition(playerMoveAction2)

       assert(message == "Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61")
     }

     "move to 12 if the sum is at 6(the bridge)" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveAction = PlayerMoveAction(player.name, Move(2,2))
       newGame.movePlayerPosition(playerMoveAction)

       val playerMoveAction2 = PlayerMoveAction(player.name, Move(1,1))

       val message = newGame.movePlayerPosition(playerMoveAction2)

      assert(message == "Pippo rolls 1, 1. Pippo moves from 4 to The Bridge. Pippo jumps to 12")

       val seq = Seq(PlayerPosition(Move( 1, 1) ,12),PlayerPosition(Move( 1, 1) ,6),PlayerPosition(Move( 2, 2) ,4),PlayerPosition(Move( 0, 0) ,0))

       assert(newGame.getBoard.get("Pippo").contains(seq))
     }
     "move to 12 if the sum is at 6(the bridge) using a dice of 4,2" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveAction = PlayerMoveAction(player.name, Move(4,2))
       val message =  newGame.movePlayerPosition(playerMoveAction)

      assert(message == "Pippo rolls 4, 2. Pippo moves from 0 to The Bridge. Pippo jumps to 12")


       val seq = Seq(PlayerPosition(Move( 4, 2) ,12),PlayerPosition(Move( 4, 2) ,6),PlayerPosition(Move( 0, 0) ,0))

       assert(newGame.getBoard.get("Pippo").contains(seq))
     }

     "return all sequence of the goose list" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveAction1 = PlayerMoveAction(player.name, Move(2,1))
       val playerMoveAction2 = PlayerMoveAction(player.name, Move(1,1))

       newGame.movePlayerPosition(playerMoveAction1)
       newGame.movePlayerPosition(playerMoveAction2)

     val   gooseList = newGame.getGooseList(newGame.getBoard.get("Pippo").head)
       assert(gooseList ==  List(PlayerPosition(Move(1, 1),7), PlayerPosition(Move(1, 1),5)))
     }

     "double the sum if the playerPosition lands on the goose" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveAction1 = PlayerMoveAction(player.name, Move(2,1))
       val playerMoveAction2 = PlayerMoveAction(player.name, Move(1,1))

       newGame.movePlayerPosition(playerMoveAction1)
       val message = newGame.movePlayerPosition(playerMoveAction2)
      /// assert(message == "Pippo rolls 1, 1. Pippo from 3 to 5, The Goose. Pippo moves again and goes to 7")
       assert(message == "Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7")
     }
     "double the sum if the playerPosition lands on the goose with a single move to a goose" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveAction1 = PlayerMoveAction(player.name, Move(2,3))


      // newGame.movePlayerPosition(playerMoveAction1)
       val message = newGame.movePlayerPosition(playerMoveAction1)
      /// assert(message == "Pippo rolls 1, 1. Pippo from 3 to 5, The Goose. Pippo moves again and goes to 7")
       assert(message == "Pippo rolls 2, 3. Pippo moves from 0 to 5, The Goose. Pippo moves again and goes to 10")
     }

     "triple  the sum if the playerPosition lands on the goose after the previous doubled goose move" in {
       val player = AddPlayerAction("Pippo")
       val newGame = new Game
       newGame.addPlayer(player)

       val playerMoveAction1 = PlayerMoveAction(player.name, Move(2,1))
       val playerMoveAction2 = PlayerMoveAction(player.name, Move(6,1))
       val playerMoveAction3 = PlayerMoveAction(player.name, Move(2,2))

       newGame.movePlayerPosition(playerMoveAction1)
       newGame.movePlayerPosition(playerMoveAction2)
       val message = newGame.movePlayerPosition(playerMoveAction3)

       assert(message == "Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 22")
     }
   }
}
