package goosekata

import org.scalatest.WordSpec

class ActionSpec extends  WordSpec {

  "Th Action trait object " should {

    "parse a string to AddPlayerAction" in {
      val string = "add player Pippo"
      val result = Action(string)
      assert(result.contains(AddPlayerAction("Pippo")))
    }

    "parse a string to PlayerMoveAction" in {
      val string = "move Pippo 1, 2"
      val result = Action(string)
      assert(result.contains(PlayerMoveAction("Pippo", Move(1,2))))
    }

    "generate random Move if only a player name is supplied " in {
      val string = "move Pippo"
      val result = Action(string)
      assert(result.nonEmpty)
    }

  }

}
