package goosekata

import org.scalatest._

class MoveSpec extends WordSpec  {

  "The move class " should {

    "add a move to PlayerPosition " in {
      val playerPosition = PlayerPosition(Move(1,1), 2)
      val move   = Move(4,2)
      assert(move.add(playerPosition) == PlayerPosition(Move(4,2), 8))
    }
  }
}
