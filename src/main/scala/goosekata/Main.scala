package goosekata

import wvlet.log._

object Main extends  LogSupport with App {
  println("Welcome to The Goose Game Kata: \n")
  val game = new Game()
  while(game.getGameStatus) {
    val input = scala.io.StdIn.readLine()
    Action(input) match {
      case Some(action) =>
        action match {
          case x: AddPlayerAction => game.addPlayer(AddPlayerAction(x.name)) match {
            case Right(players) => println("players: " + players.mkString(",") + "\n")
            case Left(error)    => println(error + "\n")
          }
          case x:PlayerMoveAction => println(game.movePlayerPosition(PlayerMoveAction(x.name,x.move)) + "\n")
        }
      case None         => println("invalid input")
     }
    }

}
