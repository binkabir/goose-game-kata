package goosekata

/**
  *
  */
object PlayerPosition{
  def addPosition(newPos : PlayerPosition, oldPos : PlayerPosition): PlayerPosition = {
    newPos.copy(sum  = oldPos.sum + newPos.move.x + newPos.move.y)

  }
}

/**
  * case class of the player position on the board
  * @param move
  * @param sum
  */
case class PlayerPosition(move: Move, sum : Int )