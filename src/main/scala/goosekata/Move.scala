package goosekata

/**
  * case class to map the position of a player in board
  * @param x
  * @param y
  */
case class Move(x:Int, y:Int){

  def add(lastMove: PlayerPosition ) = PlayerPosition(
    move =  Move(x,y),
    sum  =  lastMove.sum + x + y
  )
 override def toString = s"$x, $y"
}
