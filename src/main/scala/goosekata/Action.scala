package goosekata

import scala.util.Random

sealed trait  Action
final case class AddPlayerAction(name: String) extends  Action
final case class PlayerMoveAction(name:String, move: Move) extends  Action


/**
  * parse strings to valid user actions
  */
object Action {
  def apply(input: String): Option[Action] = {
    try {
      val values = input.split(" ").toList

      values match {
        case List("add", "player", name) => Some(AddPlayerAction(name))
        case List("move", name, coord) =>
          string2Points(coord) match {
            case Some(move) => Some(PlayerMoveAction(name, move))
            case None => None
          }
        case List("move", name , x, y ) =>  string2Int(x) match {
          case Some(int) => if(int <= 6 && y.toInt <=  6 && int != 0 && y.toInt != 0 ) Some(PlayerMoveAction(name, Move(int,y.toInt))) else None
          case None      => None
        }
        case List("move", name )        => Some(PlayerMoveAction(name, generateRandomMove()))
        case _  => None
      }
    } catch {
      case e: Exception => None
    }
  }

  /**
    * convert strings to x,y points
    * @param string
    * @return
    */
  private[this] def string2Points(string: String): Option[Move] = {
    try {
      val points = string.split(",").map(_.toInt)
      if (points.length == 2 && points.isInstanceOf[Array[Int]]) {
        val x = points(0)
        val y = points(1)
         if( x <= 6 && y <= 6 && x != 0 && y != 0 ) Some(Move(x, y)) else None
      } else None
    } catch {
      case e: Exception => None
    }
  }

  /**
    * convert string to Int
    * @param x
    * @return
    */
  private[this] def string2Int(x: String): Option[Int] = {
    try {
      val value = x.split(",")
      value.map(_.toInt).headOption
    }catch {
      case e: Exception => None
    }
  }

  /**
    * generate random  dice Move(x,y)
    * @return
    */
  private[this] def generateRandomMove():Move = {
    val r = Random
    val randomX = r.nextInt(7)
    val randomY = r.nextInt(7)
    val x = if(randomX == 0) 1 else randomX
    val y = if(randomY == 0) 1 else randomY

    Move(x,y)
  }
}