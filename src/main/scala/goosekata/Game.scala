package goosekata

import scala.annotation.tailrec
import scala.collection.immutable._

object Game {
  private val win    = 63
  private val bridge = 6
  private val goose  = List(5,9,14,18,23,27)

}

class Game {

  import Game._
  private[this] var  board =  Map[String, Seq[PlayerPosition]]()
  private[this] var gameStatus = true

  def getBoard = board
  def getGameStatus = gameStatus
  /**
    * add a player to the game
    * @param player
    * @return
    */
  def addPlayer(player:AddPlayerAction): Either[String, Seq[String]] = {
    board.contains(player.name) match {
       case true  =>  Left(s"${player.name}: already existing player")
       case false =>
        val playerStartCoordinate = Seq(PlayerPosition(Move(0, 0), 0))
         board += (player.name -> playerStartCoordinate)
        Right(board.keys.toList)
    }
  }

  /**
    * move an existing player to a new position (sum) based on the dice rolled
    * @param playerMove
    * @return
    */
  def movePlayerPosition(playerMove: PlayerMoveAction):String = {
    board.get(playerMove.name) match {
      case Some(moveList) =>

        val newPosition = playerMove.move.add(moveList.head)
        val seq         = newPosition +: moveList

        val adjustWin = adjustScoreAboveWin(seq)
        board += (playerMove.name -> adjustWin)

        val bridgeMove = moveToBridge(adjustWin)
        board += (playerMove.name -> bridgeMove)

        val gooseMove = gooseJump(bridgeMove)
        board += (playerMove.name -> gooseMove)

        playerStatus(playerMove.name, gooseMove)

      case None           => "Invalid player"
    }
  }

  /**
    * get the list of PlayerPosition that contains the goose picture
    * @param seq
    * @return
    */
  @tailrec
  private[this] def gooseJump(seq: Seq[PlayerPosition]): Seq[PlayerPosition] ={
    seq.headOption match {
      case Some(playerPosition) =>
        val sum      = playerPosition.sum
        val sumPoint = playerPosition.move.x + playerPosition.move.y
        if(goose.contains(playerPosition.sum))
          gooseJump(playerPosition.copy(sum = sum + sumPoint) +: seq)
        else seq

      case None                => seq
    }

  }

  /**
    * Identify the playerPosition that is a bridge and add the jump to 12
    * @param seq
    * @return
    */
  private[this] def moveToBridge(seq: Seq[PlayerPosition]): Seq[PlayerPosition] ={
    seq.headOption match {
      case Some(playerPosition) =>
        if (playerPosition.sum == bridge) playerPosition.copy(sum = 12) +: seq else seq

      case None => seq
    }
  }

  /**
    * check if the playerPosition have exceed the score value move to 61
    * @param seq
    * @return seq[PlayerPosition]
    */
  private[this] def adjustScoreAboveWin(seq: Seq[PlayerPosition]): Seq[PlayerPosition] = {
     seq.headOption match {
       case Some(playerPosition) =>
          if(playerPosition.sum > win) playerPosition.copy(sum = 61 ) +: seq  else seq
       case None                 => seq
     }
  }

  /**
    * return the string of player status
    * @param name
    * @param seq
    * @return
    */
  private[this] def playerStatus(name:String , seq: Seq[PlayerPosition]): String= {

    val middleValue = seq(1)
    val leftValue   = seq(0)

    if(goose.contains(middleValue.sum)) {
      val gooseList = getGooseList(seq)
      playerWithGooseStatus(name, gooseList)
    } else if(middleValue.sum == bridge){
      val rightValue  = seq(2)
      s"$name rolls ${leftValue.move.toString}. $name moves from ${rightValue.sum } to The Bridge. $name jumps to ${leftValue.sum}"
    }
    else if(middleValue.sum > win ) {
      val rightValue  = seq(2)
      s"$name rolls ${leftValue.move.toString}. $name moves from ${rightValue.sum} to $win. $name bounces! $name returns to ${leftValue.sum}"
    } else {

      leftValue.sum match {
            case x if x == win =>
              gameStatus = false
              s"$name rolls ${leftValue.move.toString}. $name moves from ${middleValue.sum} to ${leftValue.sum}. $name Wins!!"

            case x if x < win  => s"$name rolls ${leftValue.move.toString}. $name moves from ${middleValue.sum} to ${leftValue.sum}"
            case _             => s"${name } rolls ${leftValue.move.toString}. $name moves from Start to ${leftValue.sum}"
      }
    }
  }

  /**
    * return the string message of the player status for goose positions
    * @param name
    * @param seq
    * @return
    */
  private[this] def playerWithGooseStatus(name:String , seq: Seq[PlayerPosition]): String = {

    val rightValue = seq(1)
    val leftValue  = seq(0)
    val diff       = leftValue.sum - rightValue.sum
       def isGooseElement(x: PlayerPosition) : String = {
         if(goose.contains(x.sum - diff)){
           if(goose.contains(x.sum)) s"$name moves again and goes to ${x.sum }, The Goose. " else s"$name moves again and goes to ${x.sum }"
         }else {
           s"$name rolls ${leftValue.move.toString}. $name moves from ${x.sum - diff} to ${x.sum}, The Goose. "
         }
       }

     seq.foldLeft("")( (x,y) =>  isGooseElement(y) + x )
  }

  /**
    * get the list of playPosition which have a sequence of position that are goose
    * @param list1
    * @return Seq[PlayerPosition]
    */
  def getGooseList(list1: Seq[PlayerPosition]) : Seq[PlayerPosition] = {
    val tail = list1.tail

    def findGoose(list3: Seq[PlayerPosition], list4: Seq[PlayerPosition]): Seq[PlayerPosition]={
      if(list3.nonEmpty && goose.contains(list3.head.sum)){
        findGoose(list3.tail, list4 :+ list3.head)
      } else {
        list4
      }
    }
      list1.head +: findGoose(tail, Seq[PlayerPosition]())
  }
}

